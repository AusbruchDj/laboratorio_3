import serial
import numpy
import matplotlib.pyplot as plt
import numpy.matlib as npm
from copy import copy,deepcopy
import time
import math
import requests
#envio de datos del giroscopio y acelerometro calibrados API1 
myAPI='KDN2KJ3UPD5X0XEF'
baseURL='https://api.thingspeak.com/update?api_key=%s'%myAPI
#Envio de datos angulos ROLL,PITCH y filtro complementrio API2
myAPI2='L532PM4BS1XTVB61'
baseURL2='https://api.thingspeak.com/update?api_key=%s'%myAPI2

Offsets = [304.96, 283.08, 4026.16, 696.94, -208.87, -55.1]
SENSITIVITY_ACCEL=2.0/32768.0
SENSITIVITY_GYRO=250.0/32768.0

Data=serial.Serial('/dev/ttyACM0',9600,timeout=10)
print (Data)

A = 0.6
B =0.4
dt = 0.01
rad2deg = 180/3.141592
print (Data)
raw=300
Datos=numpy.zeros((raw,8))
Datos1=numpy.zeros((raw,8))
Datos2=numpy.zeros((raw,8))
Roll=numpy.zeros(((raw+1),4))
Pitch=numpy.zeros(((raw+1),4))

valor=input("\n Quiere iniciar con la adquisición de datos S/N \n\n ")

if valor=='S' or valor=='s':
    print("\n Obteniendo los datos \n")
    Data.write (b'I')
    for i in range (raw):
        rec=Data.readline()
        rec=rec.decode('utf-8')
        rec=rec.split()
        Datos[i][:]=rec
    print("\n Fin de la adquisicion de datos \n")
    Datos2=deepcopy(Datos)
    #Calibración del Giroscopio y Acelerometro 
    for y in range(0,3):
        for z in range(0,raw):
            Datos2[z] [y+2]=((Datos2[z,y+2])-Offsets[y])*SENSITIVITY_ACCEL
            Datos2[z] [y+5]=((Datos2[z,y+5])-Offsets[y+3])*SENSITIVITY_GYRO

             #Plotear datos del acelerometro calibrado
    v3=plt.figure(3)
    ax3=v3.subplots(2,2)
    v3.suptitle('Acelerometro calibrado MPU6050')
    ax3[0,0].plot(Datos2[:,0],Datos2[:,2])
    ax3[0,0].set_title('ax')
    ax3[0,1].plot(Datos2[:,0],Datos2[:,3])
    ax3[0,1].set_title('ay')
    ax3[1,0].plot(Datos2[:,0],Datos2[:,4])
    ax3[1,0].set_title('az')
    ax3[1,1].plot(Datos2[:,0],Datos2[:,2],label='ax')
    ax3[1,1].plot(Datos2[:,0],Datos2[:,3],label='ay')
    ax3[1,1].plot(Datos2[:,0],Datos2[:,4],label='az')
    ax3[1,1].set_title('ax,ay y az')
    ax3[1,1].legend(loc='lower right')
    v3.show()
    #Plotear datos del giroscopio calibrado
    v4=plt.figure(4)
    ax4=v4.subplots(2,2)
    v4.suptitle('Giroscopío calibrado MPU6050')
    ax4[0,0].plot(Datos2[:,0],Datos2[:,5])
    ax4[0,0].set_title('gx')
    ax4[0,1].plot(Datos2[:,0],Datos2[:,6])
    ax4[0,1].set_title('gy')
    ax4[1,0].plot(Datos2[:,0],Datos2[:,7])
    ax4[1,0].set_title('gz')
    ax4[1,1].plot(Datos2[:,0],Datos2[:,5],label='gx')
    ax4[1,1].plot(Datos2[:,0],Datos2[:,6],label='gy')
    ax4[1,1].plot(Datos2[:,0],Datos2[:,7],label='gz')
    ax4[1,1].set_title('gx,gy y gz')
    ax4[1,1].legend(loc='lower left')
    v4.show() 
    Roll[raw][0]=raw
    Pitch[raw][0]=raw
    for i in range(0,raw):
        Roll[i][0]=i
        Pitch[i][0]=i
	#Acelerometro
        Roll[i+1][1]=(math.atan2(Datos2[i,3],Datos2[i,4]))*rad2deg
        Pitch[i+1][1]=(math.atan2(-Datos2[i,2],math.sqrt((Datos2[i,3]*Datos2[i,3])+(Datos2[i,4]*Datos2[i,4]))))*rad2deg 	
	#Giroscopio
        Roll[i+1][2] = Roll[i][3]+((Datos2[i,5]*dt)*rad2deg)
        Pitch[i+1][2] = Pitch[i][3]+((Datos2[i,6]*dt)*rad2deg)
	#Filtro complementario
        Roll[i+1][3] = (A*Roll[i+1][2])+(B*Roll[i+1][1])
        Pitch[i+1][3] = (A*Pitch[i+1][2])+(B*Pitch[i+1][1])

        v5=plt.figure(5)
        ax5=v5.subplots(2,2)
        v5.suptitle('Angulo ROLL')
        ax5[0,0].plot(Roll[:,0],Roll[:,1])
        ax5[0,0].set_title('Roll Acelerometro')
        ax5[0,0].set_xlabel('Muestras')
        ax5[0,0].set_ylabel('Grados')
        ax5[0,1].plot(Roll[:,0],Roll[:,2])
        ax5[0,1].set_title('Roll Giroscopio')
        ax5[0,1].set_xlabel('Muestras')
        ax5[0,1].set_ylabel('Grados')
        ax5[1,0].plot(Roll[:,0],Roll[:,3])
        ax5[1,0].set_title('Roll Complementario')
        ax5[1,0].set_xlabel('Muestras')
        ax5[1,0].set_ylabel('Grados')
        ax5[1,1].plot(Roll[:,0], Roll[:,(1,2,3)])
        ax5[1,1].set_title('Roll Acel, Gyro y FC')
        ax5[1,1].set_xlabel('Muestras')
        ax5[1,1].set_ylabel('Grados')
        v5.show()
        #Ploter del Angulo PITCH
        v6=plt.figure(6)
        ax6=v6.subplots(2,2)
        v6.suptitle('Angulo PITCH')
        ax6[0,0].plot(Pitch[:,0],Pitch[:,1])
        ax6[0,0].set_title('Pitch Acelerometro')
        ax6[0,0].set_xlabel('Muestras')
        ax6[0,0].set_ylabel('Grados')
        ax6[0,1].plot(Pitch[:,0],Pitch[:,2])
        ax6[0,1].set_title('Pitch Giroscopio')
        ax6[0,1].set_xlabel('Muestras')
        ax6[0,1].set_ylabel('Grados')
        ax6[1,0].plot(Pitch[:,0],Pitch[:,3])
        ax6[1,0].set_title('Roll Complementario')
        ax6[1,0].set_xlabel('Muestras')
        ax6[1,0].set_ylabel('Grados')
        ax6[1,1].plot(Pitch[:,0], Pitch[:,(1,2,3)])
        ax6[1,1].set_title('Pitch Acel, Gyro y FC')
        ax6[1,1].set_xlabel('Muestras')
        ax6[1,1].set_ylabel('Grados')
        v6.show()

    for i in range (raw):
        
        API1=requests.get(baseURL +'&field1='+ str(Datos2[i,2]) +'&field2='+ str(Datos2[i,3]) +'&field3='+ str(Datos2[i,4])
                          +'&field4='+ str(Datos2[i,5])+'&field5='+ str(Datos2[i,6])+'&field6='+ str(Datos2[i,7]))
        
        API2=requests.get(baseURL2 +'&field1='+str(Roll[i+1][1])+'&field2='+str(Pitch[i+1][1])+'&field3='+str(Roll[i+1][2])+'&field4='+str(Pitch[i+1][2])
                          +'&field5='+str(Roll[i+1][3])+'&field6='+str(Pitch[i+1][3]))
        print('Ax_c: %4.f, Ay_c: %4.f, Az_c: %4.f, Gx_c: %4.f,Gy_c: %4.f, Gz_c: %4.f, A_ROLL: %4.f, A_PITCH: %4.f, G_ROLL: %4.f, G_PITCH: %4.f, FC_ROLL: %4.f, FC_PITCH: %4.f,'
              %(Datos2[i,2],Datos2[i,3],Datos2[i,4],Datos2[i,5],Datos2[i,6],Datos2[i,7],Roll[i+1][1],Pitch[i+1][1],Roll[i+1][2],Pitch[i+1][2],Roll[i+1][3],Pitch[i+1][3]))
        API1.close()
        API2.close()
else:
    print("Fin de la conexion, envio de datos terminado")

